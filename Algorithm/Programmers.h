﻿#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Programmers
{

public:

	/**
	* 두정수 사이의 합.
	* @see https://programmers.co.kr/learn/courses/30/lessons/12912
	*/
	long long Level01_12912(int a, int b);


};
