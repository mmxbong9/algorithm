﻿#include "stdafx.h"
#include "Programmers.h"

/**
* 두정수 사이의 합.
* 문제 설명
* 두 정수 a, b가 주어졌을 때 a와 b 사이에 속한 모든 정수의 합을 리턴하는 함수, solution을 완성하세요.
* 예를 들어 a = 3, b = 5인 경우, 3 + 4 + 5 = 12 이므로 12를 리턴합니다.
*
* 제한 조건
* a와 b가 같은 경우는 둘 중 아무 수나 리턴하세요.
* a와 b는 -10,000,000 이상 10,000,000 이하인 정수입니다.
* a와 b의 대소관계는 정해져있지 않습니다.
*/
long long Programmers::Level01_12912(int a, int b)
{
	long long answer = 0;

	/**
	* 숏코딩 분석.
	* if (a > b) a ^= b ^= a ^= b;
	* answer = (long long)b * -~b / 2 - (long long)a * ~- a / 2;
	*/

	cout << "01. InputValue>> " << a << " :: " << b << endl;

	/**
	* a가 값이 높을때에도 낮은값에서 높은 값의 형식을 유지하기 위해,
	* b의 값이 항상 높은 값이 되도록 변수간 비트 연산을 통해 반전처리를 하게됨.
	* 예를 들어 (5, 3) 같이 a가 큰 값이 입력 되었을 경우에 (3, 5) 가 들어온것으로 설정을 위함.
	*/
	if (a > b)
	{
		// xor 연산을 통한 a와 b의 값 swap.
		a ^= b ^= a ^= b;
		cout << "02-1. b is bigger need to XOR Operation>> " << a << " :: " << b << endl;
	}

	int _b = 0; // 작은 수.
	int _a = 0; // 큰 수.
	int _result = 0; // 결과 값.

	int calc_b = -~b; // +일때 비트 반전 연산을 하게되면 값이 하나가 더해지는 성질을 이용 해서 (b+1) 하는 효과와 동일한 코드를 간략화 한것임. 즉 5의 값을 6으로 설정.
	int calc_a = ~- a; // -일때 비트 반전 연산을 하게되면 값이 하나가 빠지는 성질을 이용 해서 (a-1) 하는 효과와 동일한 코드를 간략화 한것임. 즉 3의 값을 2로 설정.

	cout << "02-2. -~b = " << calc_b << ", ~-a = " << calc_a << endl;
	cout << "02-3. b = " << b << ", a = " << a << endl;

	_b = b * calc_b / 2; // 높은 수 인 b * (b+1) / 2 : 자연수 1에서부터 b까지 더한값. 입력값이 5라면, 1+2+3+4+5 의 값을 구하는 공식.
	_a = a * calc_a / 2; // 낮은 수 인 a * (b-1) / 2 : 자연수 1에서부터 a까지 더한값. 입력값이 3이라면, 1+2+3 의 값을 구하는 공식.

	cout << "02-4. _b = " << b << " * " << calc_b << " / 2 " << endl;
	cout << "02-5. _a = " << a << " * " << calc_a << " / 2 " << endl;

	_result = _b - _a;

	cout << "03. 큰값 - 작은값 = " << _result << " > 큰값 = " << _b << ", 작은값 = " << _a << endl;

	// 콘솔 꺼지지 않도록 인풋 대기 상태로..
	string TempStr = "";
	cin >> TempStr;

	answer = _result;
	return answer;
}
